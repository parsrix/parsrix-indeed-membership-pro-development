$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });
// delay anmation for buttons
$("input,#Gender,#Town,.checkbox").on('focus' , function(e){if($(this).hasClass("error-input"))$(this).removeClass("error-input tada animated")});
// $("#Gender").on('focus' , function(e){if($(this).hasClass("error-input"))$(this).removeClass("error-input tada animated")});
// $("#Town").on('focus' , function(e){if($(this).hasClass("error-input"))$(this).removeClass("error-input tada animated")});

const $spiner = $('<span class="text-white">یه لحظه صبر کن...</span><i class="fal fa-spinner-third fa-spin"></i>');
// only digits accepted functions - step 1
(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  };
}(jQuery));

    $("input[name=nation_ID]").inputFilter(function(value) {
      return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 9999999999); });

//only alphaber acceepted validator - step 1
    $("input[name=first_name]").bind('input propertychange',function(){
         if(!/^[پچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّ\s\n\r\t\d\(\)\[\]\{\}.,،;\-؛]+$/.test($(this).val())){
            new PNotify({
            title: 'اهمم',
            text: 'فقط حروف فارسی مجازه',
            type: 'info',
            animate: {
                animate: true,
                in_class: 'flipInX',
                out_class: 'bounceOut'
            }
            });
            $(this).val("");
         }
    });
    $("input[name=last_name]").bind('input propertychange',function(){
         if(!/^[پچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّ\s\n\r\t\d\(\)\[\]\{\}.,،;\-؛]+$/.test($(this).val())){
            new PNotify({
            title: 'اهمم',
            text: 'فقط حروف فارسی مجازه',
            type: 'info',
            animate: {
                animate: true,
                in_class: 'flipInX',
                out_class: 'bounceOut'
            }
            });
            $(this).val("");
         }
    });


    $("#PersonInfo").click(function (e) {
            $(this).html($spiner).delay(2000).queue(function (next) {
            $(this).html('درج اطلاعات و ادامه <i class="fal fa-arrow-left"></i>');
            next();});
        if( !$("input[name=first_name]").val()) {
                    new PNotify({
                    title: 'یه مشکل',
                    text: 'اسمتو وارد نکردی عزیزم',
                    type: 'error',
                    animate: {
                        animate: true,
                        in_class: 'flipInX',
                        out_class: 'bounceOut'
                    }
                    });
                    $("input[name=first_name]").addClass("error-input tada animated")
                    .delay(1000)
                    .queue(function (next) {$("input[name=first_name]").removeClass("tada animated");});
            } else if( !$("input[name=last_name]").val()) {
                                new PNotify({
                                title: 'برا مرحله بعد زوده',
                                text: 'هنوز نام خانوادگی خودتو وارد نکردی',
                                type: 'error',
                                animate: {
                                    animate: true,
                                    in_class: 'flipInX',
                                    out_class: 'bounceOut'
                                }
                                });
                                $("input[name=last_name]").addClass("error-input tada animated")
                                .delay(1000)
                                .queue(function (next) {$("input[name=last_name]").removeClass("tada animated");});
                    } else if( !$("input[name=nation_ID]").val()) {
                                new PNotify({
                                title: 'عه عه',
                                text: 'کدملی رو که وارد نکردی اصلا',
                                type: 'error',
                                animate: {
                                    animate: true,
                                    in_class: 'flipInX',
                                    out_class: 'bounceOut'
                                }
                                });
                                $("input[name=nation_ID]").addClass("error-input tada animated")
                                .delay(1000)
                                .queue(function (next) {$("input[name=nation_ID]").removeClass("tada animated");});
                                } else if ( $("input[name=nation_ID]").val().length < 10  ){
                                new PNotify({
                                title: 'کدملی کوتاهه',
                                text: 'کدملی که وارد کردی رقم کم داره، یه نگاه بهش بنداز',
                                type: 'error',
                                animate: {
                                    animate: true,
                                    in_class: 'flipInX',
                                    out_class: 'bounceOut'
                                }
                                });
                                $("input[name=nation_ID]").addClass("error-input tada animated")
                                .delay(1000)
                                .queue(function (next) {$("input[name=nation_ID]").removeClass("tada animated");});
                            } else if($( "select option:selected" ).val()==0) {
                                            new PNotify({
                                            title: 'اوپس',
                                            text: 'هنوز مشخص نکردی آقا هستی یا خانم',
                                            type: 'error',
                                            animate: {
                                                animate: true,
                                                in_class: 'flipInX',
                                                out_class: 'bounceOut'
                                            }
                                            });
                                            $("#Gender").removeClass("tada animated");
                                            $("#Gender").addClass("error-input tada animated")
                                            .delay(1000)
                                            .queue(function (next) {$("#Gender").removeClass("tada animated");});
                                            } else {
                                                var $active = $('.wizard .nav-tabs li.active');
                                                $active.next().removeClass('disabled');
                                                nextTab($active);
                                            }
    });


// validation for step 2
// mobile number
$("input[name=user_login]").inputFilter(function(value) {
      return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 99999999999); });
    // on click step 2
        $("#ContactInfo").click(function (e) {
            var strMail = $("input[name=user_email]").val();
            $(this).html($spiner).delay(2000).queue(function (next) {
            $(this).html('درج اطلاعات و ادامه <i class="fal fa-arrow-left"></i>');
            next();});
        if( !$("input[name=user_login]").val()) {
                    new PNotify({
                    title: 'شماره موبایل',
                    text: 'برای عضویت وارد کردن شماره موبایلت الزامیه',
                    type: 'error',
                    animate: {
                        animate: true,
                        in_class: 'flipInX',
                        out_class: 'bounceOut'
                    }
                    });
                    $("input[name=user_login]").addClass("error-input tada animated")
                    .delay(1000)
                    .queue(function (next) {$("input[name=first_name]").removeClass("tada animated");});
            }  else if ( $("input[name=user_login]").val().charAt(0)!=0){
                new PNotify({
                                title: 'صفرو نداشتی',
                                text: 'شماره موبایل مگه با صفر شروع نمیشه؟',
                                type: 'error',
                                animate: {
                                    animate: true,
                                    in_class: 'flipInX',
                                    out_class: 'bounceOut'
                                }
                                });
                                $("input[name=user_login]").addClass("error-input tada animated")
                                .delay(1000)
                                .queue(function (next) {$("input[name=user_login]").removeClass("tada animated");});
                }else if ( $("input[name=user_login]").val().charAt(1)!=9){
                    new PNotify({
                                    title: 'این چه شماره ایه؟',
                                    text: 'شماره موبایل مگه نباید با 09 شروع بشه؟',
                                    type: 'error',
                                    animate: {
                                        animate: true,
                                        in_class: 'flipInX',
                                        out_class: 'bounceOut'
                                    }
                                    });
                                    $("input[name=user_login]").addClass("error-input tada animated")
                                    .delay(1000)
                                    .queue(function (next) {$("input[name=user_login]").removeClass("tada animated");});
                }  else if ( $("input[name=user_login]").val().length < 11  ){
                                new PNotify({
                                title: 'موبایلت!',
                                text: 'شماره موبایلت رو اشتباه زدی، رقم کم داره!',
                                type: 'error',
                                animate: {
                                    animate: true,
                                    in_class: 'flipInX',
                                    out_class: 'bounceOut'
                                }
                                });
                                $("input[name=user_login]").addClass("error-input tada animated")
                                .delay(1000)
                                .queue(function (next) {$("input[name=user_login]").removeClass("tada animated");});
                            } else if ($("input[name=user_email]").val().length > 0 && ( strMail.indexOf('@')==0 || strMail.indexOf('@')== -1 || strMail.indexOf('@') > strMail.lastIndexOf('.'))){
                                new PNotify({
                                            title: 'ایمیل اشتباه',
                                            text: 'ایمیل اجباری نیست ولی حالا که وارد کردی  یه ایمیل معتبر وارد کن',
                                            type: 'error',
                                            animate: {
                                                animate: true,
                                                in_class: 'flipInX',
                                                out_class: 'bounceOut'
                                            }
                                                        });
                                            $("input[name=user_email]").removeClass("tada animated");
                                            $("input[name=user_email]").addClass("error-input tada animated")
                                            .delay(1000)
                                            .queue(function (next) {$("input[name=user_email]").removeClass("tada animated");});
                                            } else if($( "#Town option:selected" ).val()==0) {
                                                            new PNotify({
                                                            title: 'اوخ',
                                                            text: 'استان محل سکونتت رو انتخاب نکردی',
                                                            type: 'error',
                                                            animate: {
                                                                animate: true,
                                                                in_class: 'flipInX',
                                                                out_class: 'bounceOut'
                                                            }
                                                            });
                                                            $("#Town").removeClass("tada animated");
                                                            $("#Town").addClass("error-input tada animated")
                                                            .delay(1000)
                                                            .queue(function (next) {$("#Town").removeClass("tada animated");});
                                                            } else {
                                                                var $active = $('.wizard .nav-tabs li.active');
                                                                $active.next().removeClass('disabled');
                                                                nextTab($active);
                                                            }
                    });

// validation step 3 passwords
  // on click step 3
        $("#PasswordInfo").click(function (e) {
            $(this).html($spiner).delay(2000).queue(function (next) {
            $(this).html('درج اطلاعات و ادامه <i class="fal fa-arrow-left"></i>');
            next();});
        if( !$("input[name=pass1]").val()) {
                    new PNotify({
                    title: 'وارد نکردی',
                    text: 'هنوز هیچ رمز عبوری وارد نکردی، اول یه رمز عبور وارد کن',
                    type: 'error',
                    animate: {
                        animate: true,
                        in_class: 'flipInX',
                        out_class: 'bounceOut'
                    }
                    });
                    $("input[name=pass1]").addClass("error-input tada animated")
                    .delay(1000)
                    .queue(function (next) {$("input[name=pass1]").removeClass("tada animated");});
            }  else if ( !$("input[name=pass2]").val()){
                                new PNotify({
                                title: 'تایید رمز',
                                text: 'باید رمز عبورت رو تایید کنی یعنی دقیقا عین همونو بنویسی دوباره که مطمئن بشی اشتباه نزدی',
                                type: 'error',
                                animate: {
                                    animate: true,
                                    in_class: 'flipInX',
                                    out_class: 'bounceOut'
                                }
                                });
                                $("input[name=pass2]").addClass("error-input tada animated")
                                .delay(1000)
                                .queue(function (next) {$("input[name=pass2]").removeClass("tada animated");});
                            } else if( $("input[name=pass2]").val()!= $("input[name=pass1]").val()) {
                                            new PNotify({
                                            title: 'عدم تطابق',
                                            text: 'رمز عبور و تاییدش باهم تطابق ندارن، دوباره یعی کن',
                                            type: 'error',
                                            animate: {
                                                animate: true,
                                                in_class: 'flipInX',
                                                out_class: 'bounceOut'
                                            }
                                            });
                                            $("input[name=pass2]").val("");
                                            $("input[name=pass1]").val("").focus();
                                            } else {
                                                var $active = $('.wizard .nav-tabs li.active');
                                                $active.next().removeClass('disabled');
                                                nextTab($active);
                                            }
    });

        // validation step 4 law
  // on click step 4
        $("#law-accepted").click(function (e) {
            $(this).html($spiner).delay(2000).queue(function (next) {
            $(this).html('درج اطلاعات و ادامه <i class="fal fa-arrow-left"></i>');
            next();});
            if ($("#terms").prop('checked')){
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
            } else {
                new PNotify({
                    title: 'تایید قوانین',
                    text: 'اگه قوانین رو خوندی و قبول داری اون گزینه رو به نشانه تایید تیک بزن',
                    type: 'error',
                                            animate: {
                                                animate: true,
                                                in_class: 'flipInX',
                                                out_class: 'bounceOut'
                                            }
                    });
                    $(".checkbox").addClass("tada animated")
                    .delay(1000)
                    .queue(function (next) {$(".checkbox").removeClass("tada animated").focus();});
                                            }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });






    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}
