<?php

if (empty($no_load)){
	require_once '../../../wp-load.php';
	require_once 'utilities.php';
}

	require_once 'public/mellat/lib/nusoap.php';

ini_set('display_errors','on');

//insert this request into debug payments table
if (get_option('ihc_debug_payments_db')){
	ihc_insert_debug_payment_log('mellat', $_POST);
}


Ihc_User_Logs::write_log( __('Mellat Payment IPN: Start process', 'ihc'), 'payments');
		
	if ( is_user_logged_in() ) {
		$userID = get_current_user_id();
	} else {
		session_start();
		$userID = $_SESSION['userid'];
	}

	$transfered_data	= get_option('Ihc_mellat_transfer');
	$myuserid			= $transfered_data[ $userID ]['userid'];
	$levelid			= $transfered_data[ $userID ]['levelid'];
	$namespace = "http://interfaces.core.sw.bps.com/";
	Ihc_User_Logs::write_log( __('Mellat Payment: set user id @ ', 'ihc') . $myuserid, 'payments');

	$transfered_data[$userID]  =   array();
	update_option ('Ihc_mellat_transfer',$transfered_data);
	

//		$client = new nusoap_client('https://bpm.shaparak.ir/pgwchannel/services/pgw?wsdl', 'wsdl');
        $client = new nusoap_client('https://bpm.shaparak.ir/pgwchannel/services/pgw?wsdl');
		Ihc_User_Logs::write_log( __('Mellat Payment IPN: Set live mode.', 'ihc'), 'payments');

	$params = $_REQUEST;
	/*print '<pre>';
	print_r($params);
	print '</pre>';die;*/
    $SaleOrderId = (int) @$params['SaleOrderId'];
    $RefId = @$params['RefId']; // notNeed
    $ResCode = @$params['ResCode'];
    $order_id = $_SESSION['order_id'];
    $au = $_SESSION['time'];
    $ref_num = $_POST['SaleReferenceId'];

	$mellat_email = get_option('ihc_mellat_email');
	$mellat_username = get_option('ihc_mellat_username');
	$mellat_password = get_option('ihc_mellat_password');

    $parameters = array(
        'terminalId' => $mellat_email,
        'userName' => $mellat_username,
        'userPassword' => $mellat_password,
        'orderId' => $order_id,
        'saleOrderId' => $au,
        'saleReferenceId' => $ref_num
    );

        $result = $client->call('bpVerifyRequest', $parameters, $namespace);
        $VerifyAnswer = $result;
if($VerifyAnswer == 0){
        unset($result);
        $result = $client->call('bpSettleRequest', $parameters, $namespace);
        $SetlleAnswer = $result;
        if ($SetlleAnswer == '0'){

		Ihc_User_Logs::write_log( __('Mellat Payment Is Verified. Refrence code is: ', 'ihc') . $ref_num, 'payments');
		Ihc_User_Logs::set_user_id($myuserid);
		Ihc_User_Logs::set_level_id($levelid);
		Ihc_User_Logs::write_log( __("Mellat Payment IPN: set user id @ ", 'ihc') . $myuserid, 'payments');
		Ihc_User_Logs::write_log( __("Mellat Payment IPN: Update user level expire time.", 'ihc'), 'payments');
		ihc_update_user_level_expire($level_data, $levelid, $myuserid);						
		ihc_send_user_notifications($myuserid, 'payment', $levelid);//send notification to user
		ihc_send_user_notifications($myuserid, 'admin_user_payment', $levelid);//send notification to admin
		ihc_switch_role_for_user($myuserid);
		$_POST['amount'] = $level_data['price'];
		$_POST['uid'] = $myuserid;
		$_POST['lid'] = $levelid;
		$_POST['payment_status'] = 'Completed';
		$_POST['txn_id'] = "txn_" . time() . "_" . $myuserid . "_" . $levelid;
		$_POST['ihc_payment_type'] = 'mellat';
		ihc_insert_update_transaction($myuserid, $_POST['txn_id'], $_POST);
	} else {

		Ihc_User_Logs::write_log( __('Mellat Payment IPN: request is Invaild. Error code is: ', 'ihc') . $params['ResCode'], 'payments');
		
		if (!function_exists('ihc_is_user_level_expired')){
			require_once IHC_PATH . 'public/functions.php';
		}
		$expired = ihc_is_user_level_expired($myuserid, $levelid, FALSE, TRUE);
		if ($expired){
		//it's expired and we must delete user - level relationship
			Ihc_User_Logs::write_log( __("Mellat Payment IPN: Delete user level.", 'ihc'), 'payments');
			ihc_delete_user_level_relation($levelid, $myuserid);
		}
}

	}