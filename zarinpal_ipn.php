<?php
if (empty($no_load)){
	require_once '../../../wp-load.php';
	require_once 'utilities.php';
}

require_once 'public/zarinpal/lib/nusoap.php';
	
Ihc_User_Logs::write_log( __('ZarinPal Payment IPN: Start process', 'ihc'), 'payments');

if ( is_user_logged_in() ) {
	$userID = get_current_user_id();
} else {
	session_start();
	$userID = $_SESSION['userid'];
}

$transfered_data = get_option('Ihc_zarinpal_transfer');
$myuserid = $transfered_data[ $userID ]['userid'];
$levelid = $transfered_data[ $userID ]['levelid'];
$coupon = $transfered_data[ $userID ]['coupon'];


Ihc_User_Logs::write_log( __('ZarinPal Payment: set user id @ ', 'ihc') . $myuserid, 'payments');
//$transfered_data[$userID]  =   array();
//update_option ('Ihc_zarinpal_transfer',$transfered_data);

$sandbox = get_option('ihc_zarinpal_sandbox');
$zarinpal_email = get_option('ihc_zarinpal_email');

$level_data = ihc_get_level_by_id($levelid);//getting details about current level
$r_url = get_option('ihc_zarinpal_return_page');
$Authority = htmlentities($_GET['Authority']);
$Status = htmlspecialchars($_GET['Status']);

//coupons
$coupon_data = array();
if (!empty($coupon)){
        $coupon_data = ihc_check_coupon($coupon, $levelid);
        Ihc_User_Logs::write_log( __('ZarinPal Payment: the user used the following coupon: ', 'ihc') . $coupon, 'payments');	
}

//coupon
if ($coupon_data){
    $level_data['price'] = ihc_coupon_return_price_after_decrease($level_data['price'], $coupon_data);
}


if($Status == 'OK'){
    try {
        $action = 'PaymentVerification';
        if($sandbox){
            $client = new nusoap_client('https://sandbox.zarinpal.com/pg/services/WebGate/wsdl', 'wsdl');
            $client->soap_defencoding = 'UTF-8';
            $result = $client->call($action, [
                    [
                        'MerchantID' => $zarinpal_email,
                        'Authority' => $Authority,
                        'Amount' => $level_data['price'],
                    ]
                ]);
        } else {
            $params = json_encode(array('MerchantID' => $zarinpal_email, 'Authority' => $Authority, 'Amount' => $level_data['price']));
            $ch = curl_init('https://www.zarinpal.com/pg/rest/WebGate/' . $action . '.json');
            curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($params)
            ));
            $result = json_decode(curl_exec($ch), true);
        } 
    } catch (Exception $ex) {
            Ihc_User_Logs::write_log( "exception:$ex", 'payments');
            $result = false;
    }

    Ihc_User_Logs::write_log('result: '.$result);
    if ($result['Status'] == 100 ) {
        Ihc_User_Logs::write_log( __('ZarinPal Payment Is Verified. Reference code is: ', 'ihc') . $result['RefID'], 'payments');
        Ihc_User_Logs::set_user_id($myuserid);
        Ihc_User_Logs::set_level_id($levelid);
        Ihc_User_Logs::write_log( __("ZarinPal Payment IPN: set user id @ ", 'ihc') . $myuserid, 'payments');
        Ihc_User_Logs::write_log( __("ZarinPal Payment IPN: Update user level expire time.", 'ihc'), 'payments');
        ihc_update_user_level_expire($level_data, $levelid, $myuserid);						
        ihc_send_user_notifications($myuserid, 'payment', $levelid);//send notification to user
        ihc_send_user_notifications($myuserid, 'admin_user_payment', $levelid);//send notification to admin
        ihc_switch_role_for_user($myuserid);
        $_POST['amount'] = $level_data['price'];
        $_POST['uid'] = $myuserid;
        $_POST['lid'] = $levelid;
        $_POST['payment_status'] = 'Completed';
        $_POST['txn_id'] = "txn_" . time() . "_" . $myuserid . "_" . $levelid;
        $_POST['ihc_payment_type'] = 'zarinpal';
        $_POST['ihc_authority_zarinpal'] = $Authority;
        ihc_insert_update_transaction($myuserid, $_POST['txn_id'], $_POST);
        $transfered_data[$userID]  =   array();
        update_option ('Ihc_zarinpal_transfer',$transfered_data);
    } else {
        Ihc_User_Logs::write_log( __('ZarinPal Payment IPN: request is Invaild. Error code is: ', 'ihc') . $result['Status'], 'payments');
        if (!function_exists('ihc_is_user_level_expired')){
            require_once IHC_PATH . 'public/functions.php';
        }
        $expired = ihc_is_user_level_expired($myuserid, $levelid, FALSE, TRUE);
        if ($expired){
        //it's expired and we must delete user - level relationship
            Ihc_User_Logs::write_log( __("ZarinPal Payment IPN: Delete user level.", 'ihc'), 'payments');
            ihc_delete_user_level_relation($levelid, $myuserid);
        }
    }
} 
//else {
//    Ihc_User_Logs::write_log( __("ZarinPal Payment IPN: Delete user level.", 'ihc'), 'payments');
//    ihc_delete_user_level_relation($levelid, $myuserid);
//}
