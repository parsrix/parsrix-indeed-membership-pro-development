<?php
require '../../../../wp-load.php';
require_once 'zarinpal/lib/nusoap.php';

$userid= htmlspecialchars($_GET['uid']);
$levelid= htmlspecialchars($_GET['lid']);
$dynamic_price = htmlspecialchars($_GET['ihc_dynamic_price']);
$coupon = htmlspecialchars($_GET['ihc_coupon']);

Ihc_User_Logs::set_user_id(@$userid);
Ihc_User_Logs::set_level_id(@$levelid);
Ihc_User_Logs::write_log( __('ZarinPal Payment: Start process', 'ihc'), 'payments');
$zarinpal_email = get_option('ihc_zarinpal_email');
$currency = get_option('ihc_currency');
$levels = get_option('ihc_levels');
$sandbox = get_option('ihc_zarinpal_sandbox');
$zaringate = get_option('ihc_zarinpal_zaringate');
$r_url = get_option('ihc_zarinpal_return_page');

if(!$r_url || $r_url==-1){
    $r_url = get_option('page_on_front');
}
$r_url = get_permalink($r_url);
if (!$r_url){
    $r_url = get_home_url();
}

$err = false;
//LEVEL
if (isset($levels[$levelid])){
    $level_arr = $levels[$levelid];
    if ($level_arr['payment_type']=='free' || $level_arr['price']=='') $err = true;
} else {
    Ihc_User_Logs::write_log( __('ZarinPal Payment: Level is free, no payment required.', 'ihc'), 'payments');
    $err = true;
}
// USER ID
if (isset($userid) && $userid){
    $uid = $userid;
} else {
    $uid = get_current_user_id();
}
if (!$uid){
    $err = true;
    Ihc_User_Logs::write_log( __('ZarinPal Payment: Error, user id not set.', 'ihc'), 'payments');
} else {
    Ihc_User_Logs::write_log( __('ZarinPal Payment: set user id @ ', 'ihc') . $uid, 'payments');
}

/*************************** DYNAMIC PRICE ***************************/
if (ihc_is_magic_feat_active('level_dynamic_price') && isset($dynamic_price)){
    $temp_amount = $dynamic_price;
    if (ihc_check_dynamic_price_from_user($levelid, $temp_amount)){
        $level_arr['price'] = $temp_amount;
        Ihc_User_Logs::write_log( __('ZarinPal Payment: Dynamic price on - Amount is set by the user @ ', 'ihc') . $level_arr['price'] . $currency, 'payments');
    }
}
/**************************** DYNAMIC PRICE ***************************/

if ($err){
    ////if level it's not available for some reason, go back to prev page
    Ihc_User_Logs::write_log( __('ZarinPal Payment: stop process, redirect home.', 'ihc'), 'payments');
    header( 'location:'. $r_url );
    exit();
} else {
    $custom_data = json_encode(array('user_id' => $uid, 'level_id' => $levelid));
}

$notify_url = add_query_arg('ihc_action', 'zarinpal', $r_url);

Ihc_User_Logs::write_log( __('ZarinPal Payment: set ipn url @ ', 'ihc') . $notify_url, 'payments');
$reccurrence = FALSE;
if (isset($level_arr['access_type']) && $level_arr['access_type']=='regular_period'){
    $reccurrence = TRUE;
    Ihc_User_Logs::write_log( __('ZarinPal Payment: Recurrence payment set.', 'ihc'), 'payments');
}

Ihc_User_Logs::write_log( __('ZarinPal Payment: set admin zarinpal e-mail @ ', 'ihc') . $zarinpal_email, 'payments');	

if(isset($coupon) && !empty($coupon)){
        $output['coupon'] = $coupon;
} else{ 
    $coupon = get_option('Ihc_zarinpal_transfer')[$uid]['coupon'];
    if(isset($coupon) && !empty($coupon)){
        $output['coupon'] = $coupon;
    }
}

//coupons
$coupon_data = array();
if (!empty($coupon)){
        $coupon_data = ihc_check_coupon($coupon, $levelid);
        Ihc_User_Logs::write_log( __('ZarinPal Payment: the user used the following coupon: ', 'ihc') . $coupon, 'payments');	
}

//coupon
if ($coupon_data){
    $level_arr['price'] = ihc_coupon_return_price_after_decrease($level_arr['price'], $coupon_data);
}
	

//====================== single payment

Ihc_User_Logs::write_log( __('ZarinPal Payment: amount set @ ', 'ihc') . $level_arr['price'] . $currency, 'payments');

try {
    $action = 'PaymentRequest';
    if($sandbox){
        $client = new nusoap_client('https://sandbox.zarinpal.com/pg/services/WebGate/wsdl', 'wsdl');
        $client->soap_defencoding = 'UTF-8';
        $result = $client->call($action, [
            [
                'MerchantID'     => $zarinpal_email,
                'Amount'         => urlencode($level_arr['price']),
                'Description'    => $level_arr['name'],
                'Email'          => '',
                'Mobile'         => '',
                'CallbackURL'    => $notify_url,
            ],
        ]);
    } else {
        $params = json_encode(array('MerchantID' => $zarinpal_email, 'Amount' => urlencode($level_arr['price']), 'CallbackURL' => $notify_url, 'Mobile' => '', 'Description' => $level_arr['name']));
        $ch = curl_init('https://www.zarinpal.com/pg/rest/WebGate/' . $action . '.json');
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($params)
        ));
        $result = json_decode(curl_exec($ch), true);
    }
} catch (Exception $ex) {
        Ihc_User_Logs::write_log( "exception:$ex", 'payments');
        $result = false;
}


if ($result['Status'] == 100) {
    if( $sandbox ){
        $url = 'https://sandbox.zarinpal.com/pg/StartPay/'.$result['Authority'];
    } else if($zaringate){
        $url = 'https://www.zarinpal.com/pg/StartPay/'.$result['Authority'].'/ZarinGate';
    } 
    else {
        $url = 'https://www.zarinpal.com/pg/StartPay/'.$result['Authority'];
    }
    
    $session_status = session_status();
    if($session_status == PHP_SESSION_ACTIVE){
            session_destroy();
    }
    if($session_status == PHP_SESSION_NONE){
            session_start();
            $_SESSION['userid'] = $uid;
    }
    
    $output['userid']	= $uid;
	
    $output['levelid']	= $levelid;
    
    $save_output[$uid] = $output;

    $orderid_it=ihc_Db::get_last_order_id();
    
    ihc_Db::save_udate_order_meta($order_id=$orderid_it, $meta_key="ihc_authority_zarinpal", $meta_value=$result['Authority']);

    update_option('Ihc_zarinpal_transfer',$save_output);
    header('Location:' . $url);	
}
else {
    $url = get_permalink(get_option('ihc_zarinpal_return_page'));
    $failmsg = 'کد خطا: '.$result['Status'];
    echo $failmsg;
    header('Refresh:5;' . $url);
}

Ihc_User_Logs::write_log( __('ZarinPal Payment: Request submited.', 'ihc'), 'payments');	
exit();
