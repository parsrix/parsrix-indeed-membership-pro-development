<?php 
	require '../../../../wp-load.php';
	require_once 'mellat/lib/nusoap.php';
	
	Ihc_User_Logs::set_user_id(@$_GET['uid']);
	Ihc_User_Logs::set_level_id(@$_GET['lid']);
	Ihc_User_Logs::write_log( __('Mellat Payment: Start process', 'ihc'), 'payments');
	
	$mellat_email = get_option('ihc_mellat_email');
	$mellat_username = get_option('ihc_mellat_username');
	$mellat_password = get_option('ihc_mellat_password');
	$localDate = date("Ymd");
	$localTime = date("His");
	$payerId = '0';
	$levels = get_option('ihc_levels');
	$r_url = get_option('ihc_mellat_return_page');
	
	if(!$r_url || $r_url==-1){
		$r_url = get_option('page_on_front');
	}
	$r_url = get_permalink($r_url);
	if (!$r_url){
		$r_url = get_home_url();
	}
	
		$client = new SoapClient('https://bpm.shaparak.ir/pgwchannel/services/pgw?wsdl');
		Ihc_User_Logs::write_log( __('Mellat Payment: set Live mode', 'ihc'), 'payments');
	
		
	$err = false;	
	//LEVEL
	if (isset($levels[$_GET['lid']])){
		$level_arr = $levels[$_GET['lid']];
		if ($level_arr['payment_type']=='free' || $level_arr['price']=='') $err = true;
	} else {
		Ihc_User_Logs::write_log( __('Mellat Payment: Level is free, no payment required.', 'ihc'), 'payments');
		$err = true;
	}
	// USER ID
	if (isset($_GET['uid']) && $_GET['uid']){
		$uid = $_GET['uid'];
	} else {
		$uid = get_current_user_id();
	}
	if (!$uid){
		$err = true;	
		Ihc_User_Logs::write_log( __('Mellat Payment: Error, user id not set.', 'ihc'), 'payments');
	} else {
		Ihc_User_Logs::write_log( __('Mellat Payment: set user id @ ', 'ihc') . $uid, 'payments');
	}
	
	
	/*************************** DYNAMIC PRICE ***************************/
	if (ihc_is_magic_feat_active('level_dynamic_price') && isset($_GET['ihc_dynamic_price'])){
		$temp_amount = $_GET['ihc_dynamic_price'];
		if (ihc_check_dynamic_price_from_user($_GET['lid'], $temp_amount)){
			$level_arr['price'] = $temp_amount;
			Ihc_User_Logs::write_log( __('Mellat Payment: Dynamic price on - Amount is set by the user @ ', 'ihc') . $level_arr['price'] . $currency, 'payments');
		}
	}
	/**************************** DYNAMIC PRICE ***************************/
		
	if ($err){
		////if level it's not available for some reason, go back to prev page
		Ihc_User_Logs::write_log( __('Mellat Payment: stop process, redirect home.', 'ihc'), 'payments');
		header( 'location:'. $r_url );
		exit();
	} else {
		$custom_data = json_encode(array('user_id' => $uid, 'level_id' => $_GET['lid']));
	}
	
//	$notify_url = str_replace('public/', 'mellat_ipn.php', plugin_dir_url(__FILE__));
	$site_url = site_url();
	$site_url = trailingslashit($site_url);
	/***************/
	$notify_url = add_query_arg('ihc_action', 'mellat', $r_url);
	//print $notify_url;die;
	Ihc_User_Logs::write_log( __('Mellat Payment: set ipn url @ ', 'ihc') . $notify_url, 'payments');
	
	$reccurrence = FALSE;
	if (isset($level_arr['access_type']) && $level_arr['access_type']=='regular_period'){
		$reccurrence = TRUE;
		Ihc_User_Logs::write_log( __('Mellat Payment: Recurrence payment set.', 'ihc'), 'payments');		
	}
	

	Ihc_User_Logs::write_log( __('Mellat Payment: set admin mellat e-mail @ ', 'ihc') . $mellat_email, 'payments');	
		
	//coupons
	$coupon_data = array();
	if (!empty($_GET['ihc_coupon'])){
		$coupon_data = ihc_check_coupon($_GET['ihc_coupon'], $_GET['lid']);
		Ihc_User_Logs::write_log( __('Mellat Payment: the user used the following coupon: ', 'ihc') . $_GET['ihc_coupon'], 'payments');	
	}
	

		//====================== single payment
		
		//coupon
		if ($coupon_data){
			$level_arr['price'] = ihc_coupon_return_price_after_decrease($level_arr['price'], $coupon_data);
		}
					
		
		Ihc_User_Logs::write_log( __('Mellat Payment: amount set @ ', 'ihc') . $level_arr['price'] . $currency, 'payments');	
			
//		$client->soap_defencoding = 'UTF-8';
//print 	$notify_url;die;
        $additionalData = "Pay";
        $callBackUrl = $notify_url;
        $payerId = 0;
        $t = time();
        $date = date("Ymd");
        $time = date("His");
        $parameters = array(
            'terminalId' => $mellat_email,
            'userName' => $mellat_username,
            'userPassword' => $mellat_password,
            'orderId' => $t,
            'amount' => $level_arr['price'],
            'localDate' => $date,
            'localTime' => $time,
            'additionalData' => $additionalData,
            'callBackUrl' => $callBackUrl,
            'payerId' => $payerId
        );
        $result = $client->bpPayRequest($parameters);

        $check = explode(",", $result->return);


        if ($check[0] == "0") {
				//$url = 'https://bpm.shaparak.ir/pgwchannel/startpay.mellat"'.$result['Authority'];
            $action = 'https://bpm.shaparak.ir/pgwchannel/startpay.mellat';
			
			session_start();

            $_SESSION['userid'] = $uid;

            $_SESSION['order_id'] = $check[1];
            $_SESSION['time'] = $t;

			$output['userid']	= $uid;
			$output['levelid']	= $_GET['lid'];

			$save_output[$uid]   =   $output;
			update_option('Ihc_mellat_transfer',$save_output);

            $form = "<html>";
            $form .= "<body onLoad=\"document.forms['sendparams'].submit();\" >";
            $form .= "<form name=\"sendparams\" method=\"POST\" action=\"$action\" enctype=\"application/x-www-form-urlencoded\" >\n";
            $form .= "<input type=\"hidden\" name=\"RefId\" value=\"{$check[1]}\" />\n";
            $form .= "</form>";
            $form .= "Please Wait...";
            $form .= "</body>";
            $form .= "</html>";

            echo $form;
			//header('Location:' . $url);
			
		} else {
			$url = get_permalink(get_option('ihc_mellat_return_page'));
			$failmsg = 'کد خطا: '.$check[0];
			echo $failmsg;
			header('Refresh:5;' . $url);
		}
	
	Ihc_User_Logs::write_log( __('Mellat Payment: Request submited.', 'ihc'), 'payments');		
	exit();
	
	