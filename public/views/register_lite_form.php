<div class="iump-register-form  <?php echo @$data['template'];?>">
	<?php do_action('ihc_print_content_before_register_lite_form');?>
	<link rel="stylesheet" href="<?= IHC_URL . 'assets/css/animate.css';?>">
	<link rel="stylesheet" href="<?= IHC_URL . 'assets/css/register.css';?>">
	<link rel="stylesheet" href="<?= IHC_URL . 'assets/css/pnotify.custom.css';?>">
	<form action="" method="post" name="createuser" id="createuser" class="ihc-form-create-edit" enctype="multipart/form-data" >

		<div class="row">
		        <section>
		        <div class="wizard">
		            <div class="wizard-inner">
		                <div class="connecting-line"></div>
		                <ul class="nav nav-tabs" role="tablist">

		                    <li role="presentation" class="active">
		                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="ورود اطلاعات فردی">
		                            <span class="round-tab">
		                                <i class="fal fa-user-circle fa-lg"></i>
		                            </span>
		                        </a>
		                    </li>

		                    <li role="presentation" class="disabled">
		                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="ورود اطلاعات تماس">
		                            <span class="round-tab">
		                                <i class="fal fa-envelope fa-lg"></i>
		                            </span>
		                        </a>
		                    </li>
		                    <li role="presentation" class="disabled">
		                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="ورود گذرواژه">
		                            <span class="round-tab">
		                                <i class="fal fa-unlock-alt fa-lg"></i>
		                            </span>
		                        </a>
		                    </li>

		                    <li role="presentation" class="disabled">
		                        <a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="تایید قوانین">
		                            <span class="round-tab">
		                                <i class="fal fa-balance-scale fa-lg"></i>
		                            </span>
		                        </a>
		                    </li>
		                    <li role="presentation" class="disabled">
		                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="انتخاب سطح و پرداخت">
		                            <span class="round-tab">
		                                <i class="fal fa-credit-card fa-lg"></i>
		                            </span>
		                        </a>
		                    </li>
		                </ul>
		            </div>

		            <form role="form" action="#">
		                <div class="tab-content">
		                    <div class="tab-pane active" role="tabpanel" id="step1">
		                        <div class="row">
		                            <div class="col-xs-12 col-md-8">
		                                  <div class="row user-perdonal-info">
		                                    <div class="col-xs-12">
		                                        <h3>اطلاعات فردی</h3>
		                                        <p>سلام دوست خوبم، از اینکه تصمیم گرفتی پارس ریکسی بشی متشکریم، ابتدا برای عضو شدن باید اطلاعات فردی خودت رو اینجا وارد کنی تا ما بهتر بشناسیمت.</p>
		                                    </div>
																				<div class="col-xs-12 col-sm-6">
	                                      	<input type="text" name="first_name" class="form-control" placeholder="نام">
		                                    </div>
		                                    <div class="col-xs-12 col-sm-6">
		                                      <input type="text" name="last_name" class="form-control" placeholder="نام خانوادگی">
		                                    </div>
		                                    <div class="col-xs-12 col-sm-6">
		                                      <input type="text" name="nation_ID" maxlength="10" class="form-control" placeholder="کد ملی">
		                                    </div>
		                                    <div class="col-xs-12 col-sm-6">
		                                       <div class="form-group">
		                                            <select class="form-control" name="gender" id="Gender">
		                                            <option value="0" selected>جنسیت خودتو انتخاب کن</option>
		                                            <option value="1">آقا هستم</option>
		                                            <option value="2">خانم هستم</option>
		                                          </select>
		                                        </div>
		                                    </div>
		                                  </div>
		                            </div>
		                            <div class="col-xs-12 col-md-4">
		                                <img src="<?= IHC_URL . 'assets/images/reg/register-personaldata.svg';?>" alt="register personal data" class="form-img">
		                            </div>
		                        </div>
		                        <ul class="list-inline pull-left form-navigation">
		                            <li><button type="button" class="btn btn-purple" id="PersonInfo">
		                            درج اطلاعات و ادامه
		                            <i class="fal fa-arrow-left mr-2"></i>
		                        </button></li>
		                        </ul>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step2">
		                        <div class="row">
		                            <div class="col-xs-12 col-md-8">
		                                <div class="col-xs-12">
		                                        <h3>اطلاعات تماس</h3>
		                                        <p>خب! توی این مرحله نوبت میرسه به درج اطلاعات تماس شما، که بتونیم باهم در ارتباط باشیم و از اخبار و اطلاعات گرفته تا رویداد ها و الباقی چیزا رو به دستت برسونیم.</p>
		                                </div>
		                                <div class="row user-perdonal-info">
		                                    <div class="col-xs-12 col-sm-6">
		                                      <input type="text" name="user_login" class="form-control" placeholder="شماره موبایل">
		                                    </div>
		                                    <div class="col-xs-12 col-sm-6">
		                                      <input type="email" name="user_email" class="form-control" placeholder="آدرس ایمیل">
		                                    </div>
		                                    <div class="col-xs-12 col-sm-6">
		                                      <input type="text" name="Instagram_Id" class="form-control" placeholder="آی دی اینستاگرام">
		                                    </div>
		                                    <div class="col-xs-12 col-sm-6">
		                                       <div class="form-group">
		                                          <select name="town" class="form-control" id="Town">
		                                            <option value="0" selected>استان محل سکونت</option>
																								<option value="1">یزد</option>
		                                            <option value="2">تهران</option>
		                                          </select>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="col-xs-12 col-md-4">
		                                <img src="<?= IHC_URL . 'assets/images/reg/register-contactinfo.svg';?>" alt="register contact info" class="form-img">
		                            </div>
		                        </div>
		                        <ul class="list-inline pull-left form-navigation">
		                            <li><button type="button" class="btn btn-secondary prev-step">
		                            <i class="fal fa-arrow-right ml-2"></i>
		                            بازگشت
		                        </button></li>
		                            <li><button type="button" class="btn btn-purple" id="ContactInfo">
		                                درج اطلاعات و ادامه
		                                <i class="fal fa-arrow-left mr-2"></i>
		                            </button></li>
		                        </ul>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step3">
		                        <div class="row">
		                            <div class="col-xs-12 col-md-7">
		                                  <div class="row user-perdonal-info">
		                                    <div class="col-xs-12">
		                                        <h3>رمز عبور</h3>
		                                        <p>برای اینکه بتونی دفعات بعدی وارد سایت بشی به یه رمز عبور نیاز داری، پس توی فیلد های زیر وارد کن، یادت باشه رمز رو هم سخت بذاری هم یادت بمونه همیشه، ولی اگه یادت رفت هیچ اشکالی نداره <a href="https://parsrix.com/بازیابی-رمزعبور">ما اینجا هستیم</a>.

		                                        </p>
		                                    </div>
		                                    <div class="col-xs-12">
		                                      <input type="password" name="pass1" class="form-control" placeholder="رمز عبور">
		                                    </div>
		                                    <div class="col-xs-12">
		                                      <input type="password" name="pass2" class="form-control" placeholder="تایید رمز عبور">
		                                    </div>
		                                  </div>
		                            </div>
		                            <div class="col-xs-12 col-md-5">
		                                <img src="<?= IHC_URL . 'assets/images/reg/register-pasword.svg';?>" alt="register password" class="form-img">
		                            </div>
		                        </div>
		                        <ul class="list-inline pull-left form-navigation">
		                            <li><button type="button" class="btn btn-secondary prev-step">
		                                <i class="fal fa-arrow-right ml-2"></i>
		                                بازگشت
		                            </button></li>
		                            <li><button type="button" class="btn btn-purple" id="PasswordInfo">
		                                درج اطلاعات و ادامه
		                                <i class="fal fa-arrow-left"></i>
		                            </button></li>
		                        </ul>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step4">
		                        <div class="row">
		                            <div class="col-xs-12 col-md-8">
		                                <div class="col-xs-12">
		                                        <h3>بایدها و نبایدها</h3>
		                                        <p>از اونجایی که اینجا آدم های زیادی عضو هستن و نباید حقی از کسی ضایع بشه، یه سری قوانین وضع شدن که با عضویت توی سامانه ملزم به رعایت اون هستی، پس بهتره یه نگاهی بهشون بندازی.</p>
		                                        <h3>قوانین و سیاست های حفظ حریم خصوصی</h3>
		                                        <p>
		                                        کاربر گرامی لطفاً موارد زیر را جهت استفاده بهینه از خدمات به دقت مطالعه فرمایید. ورود کاربران به سامانه پارسریکس هنگام استفاده از پروفایل شخصی، طرح‏‌های تشویقی و سایر خدمات ارائه شده توسط این سامانه  به معنای آگاه بودن و پذیرفتن شرایط و قوانین و همچنین نحوه استفاده از سرویس‌‏ها و خدمات آن است. توجه داشته باشید که عضویت در این سامانه نیز در هر زمان به معنی پذیرفتن کامل کلیه شرایط و قوانین سامانه پارس ریکس از سوی کاربر است. لازم به ذکر است شرایط و قوانین مندرج، جایگزین کلیه توافق‏‌های قبلی محسوب می‏‌شود.
		                                        </p>
		                                        <h3>قوانین عمومی</h3>
		                                        <p>
		                                        توجه داشته باشید کلیه اصول و رویه‏‌های سامانه پارسریکس منطبق با قوانین جمهوری اسلامی ایران، قانون تجارت الکترونیک و قانون حمایت از حقوق مصرف کننده است و متعاقبا کاربر نیز موظف به رعایت قوانین مرتبط با کاربر است. در صورتی که در قوانین مندرج، رویه‏‌ها و سرویس‏‌های این سامانه تغییراتی در آینده ایجاد شود، در همین صفحه منتشر و به روز رسانی می شود و شما توافق می‏‌کنید که استفاده مستمر شما از سایت به معنی پذیرش هرگونه تغییر است.
		                                        </p>
		                                        <h3>تعریف مشتری یا کاربر</h3>
		                                        <p>
		                                        مشتری یا کاربر به شخصی گفته می‌شود که با اطلاعات کاربری خود که در فرم ثبت نام درج کرده است، به ثبت سفارش یا هرگونه استفاده از خدمات سامانه پارس ریکس اقدام کند.
		                                        همچنین از آنجا که وب سایت سامانه پارسریکس یک وب‌سایت ارائه دهنده خدمات رفاهی است، طبق قانون تجارت الکترونیک مشتری یا مصرف کننده هر شخصی است که به منظوری جز تجارت یا شغل حرفه‌ای به عضویت یا خرید کالا یا خدمات اقدام می‌کند.
		                                        </p>
		                                        <h3>ارتباطات الکترونیکی</h3>
		                                        <p>
		                                        هنگامی که شما از سرویس‌‏ها و خدمات سامانه پارسریکس استفاده می‏‌کنید، سفارش اینترنتی خود را ثبت یا خرید می‏‌کنید، این ارتباطات به صورت الکترونیکی انجام می‏‌شود و در صورتی که درخواست شما با رعایت کلیه اصول و رویه‏‌ها باشد، شما موافقت می‌‏کنید که سامانه پارس ریکس به صورت الکترونیکی (از طریق پست الکترونیکی، سرویس پیام کوتاه و سایر سرویس‌های الکترونیکی) به درخواست شما پاسخ دهد.
		                                        همچنین آدرس ایمیل و تلفن‌هایی که مشتری در پروفایل خود ثبت می‌کند، تنها آدرس ایمیل و تلفن‌های رسمی و مورد تایید مشتری است و تمام مکاتبات و پاسخ‌های مجموعه از طریق آنها صورت می‌گیرد. جهت اطلاع رسانی رویدادها، خدمات و سرویس‌های ویژه یا پروموشن‌ها، امکان دارد سامانه پارسریکس برای اعضای وب سایت ایمیل یا پیامک ارسال نماید. در صورتی که کاربران تمایل به دریافت اینگونه ایمیل و پیامک‌ها نداشته باشند، می‌توانند درخواست لغو خبرنامه را به اطلاع پشتیبانی برسانند.</p>

		                                        <h3>سیاست‏‌های رعایت حریم شخصی</h3>
		                                        <p>
		                                        سامانه پارسریکس به اطلاعات خصوصی اشخاصى که از خدمات سایت استفاده می‏‌کنند، احترام گذاشته و از آن محافظت می‏‌کند. سامانه پارسریکس متعهد می‏‌شود در حد توان از حریم شخصی شما دفاع کند و در این راستا، تکنولوژی مورد نیاز برای هرچه مطمئن‏‌تر و امن‏‌تر شدن استفاده شما از سایت را توسعه دهد. در واقع با استفاده از سامانه پارسریکس، شما رضایت خود را از این سیاست نشان می‏‌دهید. در صورت استفاده از هر یک از خدمات سامانه پارسریکس، کاربران مسئول حفظ محرمانه بودن حساب و رمز عبور خود هستند و تمامی مسئولیت فعالیت‌‏هایی که تحت حساب کاربری و یا رمز ورود انجام می‏‌پذیرد به عهده کاربران است. تنها مرجع رسمی مورد تایید ما برای ارتباط با شما، پایگاه رسمی این سایت یعنیwww.parsrix.com  است.</p>
		                                </div>
		                                <div class="checkbox">
		                                    <h4>
		                                    <input type="checkbox" id="terms" value="true">
		                                    <label for="terms" >
		                                        قوانین رو خودنم و تأیید میکنم
		                                    </label>
		                                    </h4>
		                                </div>
		                            </div>
		                            <div class="col-xs-12 col-md-4">
		                                <img src="<?= IHC_URL . 'assets/images/reg/register-law.svg'; ?>" alt="register law" class="form-img">
		                            </div>
		                        </div>
		                        <ul class="list-inline pull-left form-navigation">
		                            <li><button type="button" class="btn btn-secondary prev-step">
		                              <i class="fal fa-arrow-right ml-2"></i>
		                            بازگشت
		                        </button></li>
		                            <li><button type="button" class="btn btn-purple" id="law-accepted">
		                                درج اطلاعات و ادامه
		                                 <i class="fal fa-arrow-left mr-2"></i>
		                            </button></li>
		                        </ul>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="complete">
		                        <div class="row">
		                            <div class="col-xs-12 col-md-8">
		                                <div class="col-xs-12">
		                                        <h3>پرداخت حق عضویت</h3>
		                                        <p>چیزی دیگه نمونده، برای اینکه پارس ریکسی بشی یه حق عضویت ناچیزی رو باید متقبل بشی، برای انتقال به صفحه ی پرداخت از دکمه ی اون پایین استفاده کن.</p>
		                                        <br>
		                                <h4>سطح حساب خودت رو انتخاب کن</h4>
		                                <fieldset id="plans">
		                                    <div class="radio">
		                                        <input type="radio" id="optionsRadios1" value="3" name="plans" checked>
		                                        <label for="optionsRadios1">
		                                            اشتراک عقیق (سه ماهه)
		                                        </label>
		                                    </div>
		                                    <div class="radio">
		                                        <input type="radio" id="optionsRadios2" value="6" name="plans">
		                                        <label for="optionsRadios2">
		                                            اشتراک فیروزه (شش ماهه)
		                                        </label>
		                                    </div>
		                                    <div class="radio ">
		                                        <input type="radio" id="optionsRadios3" value="9" name="plans">
		                                        <label for="optionsRadios3">
		                                            اشتراک یاقوت (نه ماهه)
		                                        </label>
		                                    </div>
		                                    <div class="radio ">
		                                        <input type="radio"  id="optionsRadios4" value="12" name="plans">
		                                        <label for="optionsRadios4">
		                                            اشتراک زمردی (12 ماهه)
		                                        </label>
		                                    </div>
		                                    <div class="radio disabled">
		                                        <input type="radio" id="optionsRadios5" value="18" name="plans"   disabled>
		                                        <label for="optionsRadios5">
		                                            اشتراک الماس (هجده ماهه)
		                                        </label>
		                                    </div>
		                                </fieldset>
		                                </div>
		                            </div>
		                            <div class="col-xs-12 col-md-4">
		                                <img src="images/register-payment.svg" alt="register payment" class="form-img">
		                            </div>
		                        </div>
		                        <fieldset id="banks-gaits">
		                            <h5>درگاه بانکی که دوس داری از طریق اون پرداخت انجام بدی رو انتخاب کن:</h5>
		                                <ul class="banks">
		                                    <li>
		                                        <div class="radio">
		                                        <input type="radio" id="bank3" value="18" name="banks-gaits" checked >
		                                            <label for="bank3">
		                                                <img class="bnk-logo" src="images/saman-e-kish-logo.svg" alt="ردگاه پرداخت سامان کیش">
		                                                <span>درگاه پرداخت الکترونیک <br> سامان کیش</span>
		                                            </label>
		                                        </div>
		                                    </li>
		                                    <li>
		                                        <div class="radio">
		                                            <input type="radio" id="bank2" value="18" name="banks-gaits">
		                                            <label for="bank2">
		                                                <img class="bnk-logo" src="images/Ap (2).svg" alt="درگاه آسان پرداخت">
		                                                <span>درگاه پرداخت الکترونیک <br> سامان پرداخت</span>
		                                            </label>
		                                        </div>
		                                    </li>
		                                    <li>
		                                        <div class="radio">
		                                            <input type="radio" id="bank1" value="18" name="banks-gaits">
		                                            <label for="bank1">
		                                                <img class="bnk-logo" src="images/mellat-logo.svg" alt="درگاه پرداخت بانک ملت">
		                                                <span>درگاه پرداخت الکترونیک <br> بانک ملت</span>
		                                            </label>
		                                        </div>
		                                    </li>
		                                </ul>
		                                </fieldset>
		                        <ul class="list-inline pull-left form-navigation">
		                            <li><button type="button" class="btn btn-danger">
		                                <i class="fal fa-times-circle ml-2"></i>
		                                انصراف
		                            </button></li>
		                            <li><button type="button" class="btn btn-success">
		                               تکمیل عضویت
		                                <i class="fal fa-arrow-left mr-2"></i>
		                            </button></li>
		                        </ul>
		                    </div>
		                    <div class="clearfix"></div>
		                </div>
		            </form>
		        </div>
		    </section>



		<div class="impu-temp7-row">
			<div class="iump-submit-form">
				<?php echo $data['submit_button'];?>
			</div>
		</div>

		<?php foreach ($data['hidden_fields'] as $hidden_field):?>
			<?php echo $hidden_field;?>
		<?php endforeach;?>

		<?php if ($data['template']==''):?>
			</div>
		<?php endif;?>

	</form>
	<?php do_action('ihc_print_content_after_register_lite_form');?>
</div>
<script src="<?= IHC_URL . 'assets/js/register.js';?>" ></script>
<script src="<?= IHC_URL . 'assets/js/pnotify.custom.min.js';?>" ></script>
