<?php if (!empty($data['custom_css'])):?>
    <style><?php echo $data['custom_css'];?></style>
<?php endif;?>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css'>
    <link href='<?php echo IHC_URL . 'assets/css/croppic.css';?>' rel='stylesheet' type='text/css' />
    <link href='<?php echo IHC_URL . 'assets/fontawesome/css/all.min.css';?>' rel='stylesheet' type='text/css' />
    <script src="<?php echo IHC_URL . 'assets/js/jquery.mousewheel.min.js';?>"></script>
    <script src="<?php echo IHC_URL . 'assets/js/croppic.js';?>"></script>
    <script src="<?php echo IHC_URL . 'assets/js/account_page-banner.js';?>"></script>



    <script>
        IhcAccountPageBanner.init({
            triggerId					: 'js_ihc_edit_top_ap_banner',
            saveImageTarget		: '<?php echo IHC_URL . 'public/ajax-upload.php';?>',
            cropImageTarget   : '<?php echo IHC_URL . 'public/ajax-upload.php';?>',
            bannerClass       : 'ihc-user-page-top-ap-background'
        });
    </script>

<!-- deleted ehsan for profile improvement -->
    <!-- <div class="ihc-account-page-wrapp d-flex flex-column" id="ihc_account_page_wrapp"> -->
        <body class="sidebar-is-reduced">
        <header class="l-header">
            <div class="l-header__inner clearfix">
                <div class="c-header-icon js-hamburger">
                    <div class="hamburger-toggle"><span class="bar-top"></span><span class="bar-mid"></span><span class="bar-bot"></span></div>
                </div>
                <div class="c-header-brand">
                  <img class="profile-logo" src="<?php echo plugin_dir_url( __FILE__ ) . '../../assets/images/profilelogo.svg'; ?>" alt="parsrix" >
                </div>
                <div class="header-icons-group">
                    <a  href="<?php echo get_home_url(); ?>">
                        <!-- <div class="c-header-icon basket  ehsan-avatar" style="background-image: url('<?php //echo $data['avatar'];?>');"> -->
                        <div class="c-header-icon basket  ehsan-avatar" >
                             <i class="fal fa-home fa-lg"></i>
                            <!--                    <span class="c-badge c-badge--blue c-badge--header-icon animated swing">4</span>-->
                            <!--                    <i class="fa fa-shopping-basket"></i>-->
                        </div>
                    </a>
                    <a href="<?php echo get_home_url(); ?>/?ihcdologout=true">
                      <div class="c-header-icon logout">
                          <i class="fa fa-power-off"></i>
                      </div>
                    </a>
                </div>
            </div>
        </header>
        <?php
        $top_style='';
        if (empty($this->settings['ihc_ap_edit_background']) && ($this->settings['ihc_ap_top_template'] == 'ihc-ap-top-theme-2' || $this->settings['ihc_ap_top_template'] == 'ihc-ap-top-theme-3' )){
            $top_style .='style="padding-top:75px;"';
        }
        ?>

        <!--		<div class="ihc-user-page-content-wrapper  --><?php //echo @$this->settings['ihc_ap_theme'];?><!--">-->
<?php
