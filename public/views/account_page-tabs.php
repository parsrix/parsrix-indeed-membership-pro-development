<style>
	<?php foreach ($available_tabs as $slug => $array):?>
		<?php if (!empty($array['icon'])):?>
		<?php echo '.fa-' . $slug . '-account-ihc:before';?>{
			content: '\<?php echo $array['icon'];?>';
		}
		<?php endif;?>
	<?php endforeach;?>
</style>
<div class="d-flex flex-row parsrix-main-wrapper">
<div class="l-sidebar ">
    <!-- <div class="logo">
        <div class=" js-hamburger d-sm-none mr-25">
            <div class="hamburger-toggle "><span class="bar-top"></span><span class="bar-mid"></span><span class="bar-bot"></span></div>
        </div>
    </div> -->
    <div class="l-sidebar__content">
        <nav class="c-menu js-menu">
            <ul class="u-list">
                <?php if ($data['menu']):?>
                <?php foreach ($data['menu'] as $k => $array):?>
                <li class="c-menu__item <?php echo $array['class'];?>" data-toggle="tooltip" title="<?php echo $array['title'];?>">
                    <a href="<?php echo $array['url'];?>">
                        <div class="c-menu__item__inner">
                            <i class="<?php echo 'fal fa-ihc fa-' . $k . '-account-ihc fa-lg';?>"></i>
                            <div class="c-menu-item__title"><span><?php echo $array['title'];?></span></div>
                        </div>
                    </a>
                </li>
                    <?php endforeach;?>
                <?php endif;?>
<!--                <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Modules">-->
<!--                    <div class="c-menu__item__inner"><i class="fa fa-puzzle-piece"></i>-->
<!--                        <div class="c-menu-item__title"><span>Modules</span></div>-->
<!--                        <div class="c-menu-item__expand js-expand-submenu"><i class="fa fa-angle-down"></i></div>-->
<!--                    </div>-->
<!--                    <ul class="c-menu__submenu u-list">-->
<!--                        <li>Payments</li>-->
<!--                        <li>Maps</li>-->
<!--                        <li>Notifications</li>-->
<!--                    </ul>-->
<!--                </li>-->
<!--                <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Statistics">-->
<!--                    <div class="c-menu__item__inner"><i class="far fa-chart-bar"></i>-->
<!--                        <div class="c-menu-item__title"><span>Statistics</span></div>-->
<!--                    </div>-->
<!--                </li>-->
<!--                <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Gifts">-->
<!--                    <div class="c-menu__item__inner"><i class="fa fa-gift"></i>-->
<!--                        <div class="c-menu-item__title"><span>Gifts</span></div>-->
<!--                    </div>-->
<!--                </li>-->
<!--                <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Settings">-->
<!--                    <div class="c-menu__item__inner"><i class="fa fa-cogs"></i>-->
<!--                        <div class="c-menu-item__title"><span>Settings</span></div>-->
<!--                    </div>-->
<!--                </li>-->
            </ul>
        </nav>
    </div>
</div>
</body>
