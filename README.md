# Parsrix indeed-membership-pro Development

## Admin Panel user friendly:
1. [] consider two editable fields reserved and active level in zarinpal gateway
 or for all payment gateway in membership admin settings.

2. [] fix session bug on payment gateway


## Help Codes:
# change username label field name to phone number:
require_once('./wp-load.php');
$data = get_option('ihc_user_fields');
for($index = 0; $index < count($data); $index++){
    if($data[$index]['name'] == 'user_login'){
        $data[$index]['label'] = 'شماره موبایل';
        update_option('ihc_user_fields', $data);
        break;
    }
}

# Parsrix Shortcodes:
1. [ihc-tranaction-state-parsrix] => dynamic show beatiful transaction status for payment callback
2. [ihc-box-button-parsrix url="#" title="Untitled" i-class="fal fa-exclamation-triangle fa-2x" box-class="col-xs-6 col-sm-2" box-color="coral" box-text-color="white"] => show a box button
3. [ihc-open-div class='custom-class'] => print <div class="custom-class"> 
3. [ihc-close-div] => print </div>
