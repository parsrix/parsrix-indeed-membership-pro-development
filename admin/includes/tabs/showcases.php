<?php 
echo ihc_inside_dashboard_error_license();
?>
<div style="width: 97%">
	<div class="ihc-dashboard-title">
		<?php _e('Membership Pro Ultimate Wp', 'ihc');?> - 
		<span class="second-text">
			<?php _e('Front-End Showcases', 'ihc');?>
		</span>
	</div>
<div class="metabox-holder indeed">
<?php $url = get_admin_url() . 'admin.php?page=ihc_manage';?>
	<div class="ihc-popup-content showcases-wrapp" style="text-align: center;">
        	<div style="margin: 0 auto; display: inline-block;">
	            <a href="<?php echo $url.'&tab=register';?>"><div class="ihc-popup-shortcodevalue"><i class="fa-ihc fa-user-plus-ihc"></i><?php _e('Register Form', 'ihc');?><span><?php _e('Templates, Custom Fields, Special Settings, Custom Messages', 'ihc'); ?></span></div></a>
	            <a href="<?php echo $url.'&tab=login';?>"><div class="ihc-popup-shortcodevalue"> <i class="fa-ihc fa-sign-in-ihc"></i><?php _e('Login Form', 'ihc');?><span><?php _e('Templates, Display Options, Custom Messages', 'ihc'); ?></span></div></a>
	            <a href="<?php echo $url.'&tab=subscription_plan';?>"><div class="ihc-popup-shortcodevalue"> <i class="fa-ihc fa-levels-ihc"></i><?php _e('Subscription Plan', 'ihc');?><span><?php _e('Templates, Custom Style', 'ihc'); ?></span></div></a>
	            <a href="<?php echo $url.'&tab=account_page';?>"><div class="ihc-popup-shortcodevalue"> <i class="fa-ihc fa-user-ihc"></i><?php _e('Account Page', 'ihc');?><span><?php _e('Templates, ShowUp fields, ShowUp Tabs, Predefined Overview', 'ihc'); ?></span></div>  </a> 
	            <a href="<?php echo $url.'&tab=listing_users';?>"><div class="ihc-popup-shortcodevalue"> <i class="fa-ihc fa-listing_users-ihc"></i><?php _e('Members List', 'ihc');?><span><?php _e('ShortCode Generator for listing current Members ', 'ihc'); ?></span></div>  </a>          
				<div class="ihc-clear"></div>
        	</div>
    	</div>
</div>
</div>
