<?php
namespace Braintree;

/**
 * Braintree ZarinPalAccount module
 *
 * @package    Braintree
 * @category   Resources
 * @copyright  2015 Braintree, a division of ZarinPal, Inc.
 */

/**
 * Manages Braintree ZarinPalAccounts
 *
 * <b>== More information ==</b>
 *
 *
 * @package    Braintree
 * @category   Resources
 * @copyright  2015 Braintree, a division of ZarinPal, Inc.
 *
 * @property-read string $customerId
 * @property-read string $email
 * @property-read string $token
 * @property-read string $imageUrl
 */
class ZarinPalAccount extends Base
{
    /**
     *  factory method: returns an instance of ZarinPalAccount
     *  to the requesting method, with populated properties
     *
     * @ignore
     * @return ZarinPalAccount
     */
    public static function factory($attributes)
    {
        $instance = new self();
        $instance->_initialize($attributes);
        return $instance;
    }

    /* instance methods */

    /**
     * returns false if default is null or false
     *
     * @return boolean
     */
    public function isDefault()
    {
        return $this->default;
    }

    /**
     * sets instance properties from an array of values
     *
     * @access protected
     * @param array $zarinpalAccountAttribs array of zarinpalAccount data
     * @return void
     */
    protected function _initialize($zarinpalAccountAttribs)
    {
        // set the attributes
        $this->_attributes = $zarinpalAccountAttribs;

        $subscriptionArray = [];
        if (isset($zarinpalAccountAttribs['subscriptions'])) {
            foreach ($zarinpalAccountAttribs['subscriptions'] AS $subscription) {
                $subscriptionArray[] = Subscription::factory($subscription);
            }
        }

        $this->_set('subscriptions', $subscriptionArray);
    }

    /**
     * create a printable representation of the object as:
     * ClassName[property=value, property=value]
     * @return string
     */
    public function  __toString()
    {
        return __CLASS__ . '[' .
                Util::attributesToString($this->_attributes) . ']';
    }


    // static methods redirecting to gateway

    public static function find($token)
    {
        return Configuration::gateway()->payPalAccount()->find($token);
    }

    public static function update($token, $attributes)
    {
        return Configuration::gateway()->payPalAccount()->update($token, $attributes);
    }

    public static function delete($token)
    {
        return Configuration::gateway()->payPalAccount()->delete($token);
    }

    public static function sale($token, $transactionAttribs)
    {
        return Configuration::gateway()->payPalAccount()->sale($token, $transactionAttribs);
    }
}
class_alias('Braintree\ZarinPalAccount', 'Braintree_ZarinPalAccount');
